package vetoreseMatrizes;

import java.util.Scanner;

public class exercicio02 {
	static Scanner keyboard = new Scanner( System.in );
	 public static void main(String[] args) {
		 
			int[] A = new int[6];
				for (int i = 0; i < A.length; i++) {
					System.out.println("Digite o "+(i+1)+"º numero:");
					String input = keyboard.nextLine();
					A[i] = Integer.parseInt(input);
				}
			
			
			System.out.print("Valores do vetor:[");
			for (int i = 0; i < A.length; i++) {
				System.out.print(A[i]);
				if(i+1<A.length) {
					System.out.print(",");
				}
			}
			System.out.println("]");
	 }
	}

